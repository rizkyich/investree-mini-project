const findPrimeByRange = (start, finish) => {

  const primes = []

  for (let current = start; current < finish; current++) {
    if (isPrime(current)) {
      primes.push(current)
    }
  }

  return primes
}

const isPrime = (number) => {
  let start = 2
  while (start <= Math.sqrt(number)) {
    if (number % start++ < 1) return false
  }
  return number > 1
}


// Test Cases
console.log(findPrimeByRange(1, 10)) // [ 2, 3, 5, 7 ]
console.log(findPrimeByRange(11, 40)) // [11, 13, 17, 19, 23, 29, 31, 37]