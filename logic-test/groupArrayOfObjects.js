const groupArrayObjects = (array) => {
  const result = []
  let currentStr
  let count = 0

  while (count < array.length) {
    const group = {
      letter: array[count],
      sum: 0
    }

    currentStr = array[count]

    while (array[count] === currentStr) {
      group.sum++
      count++
    }

    result.push(group)
  }

  return result
}


// Test case 
console.log(groupArrayObjects(['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e'])) //[{ letter: 'a', sum: 3 }, { letter: 'b', sum: 1 }, { letter: 'c', sum: 2 }, { letter: 'b', sum: 3 }, { letter: 'd', sum: 2 }, { letter: 'e', sum: 3 }]