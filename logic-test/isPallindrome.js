const isPallindrome = (str) => {
  return typeof (str) === 'string' ? str === str.split('').reverse().join('') : 'Invalid data types'
}

// Test Cases
console.log(isPallindrome('racecar')) //true
console.log(isPallindrome('Halo saya pengen kerja')) // false
console.log(isPallindrome(9090909)) // Invalid data types
