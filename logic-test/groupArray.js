const groupArray = (array) => {
  const result = []
  let currentStr
  let count = 0

  while (count < array.length) {
    const group = []
    currentStr = array[count]

    while (array[count] === currentStr) {
      group.push(array[count])

      count++
    }

    result.push(group)
  }

  return result
}

// Test case
console.log(groupArray(['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e'])) //[['a', 'a', 'a'], ['b'], ['c', 'c'], ['b', 'b', 'b'], ['d', 'd'], ['e', 'e', 'e']]
